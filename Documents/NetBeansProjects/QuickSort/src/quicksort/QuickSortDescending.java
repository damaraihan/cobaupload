/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicksort;

/**
 *
 * @author ASUS
 */
public class QuickSortDescending {
   static void quickSort(int a[], int lo, int hi){
        //lo adalah index bawah, hi adalah index atas
        // dari bagian array yang akan di urutkan
        int i=lo, j=hi, h;
        int pivot=a[lo];
        // pembagian
        do{
            while(a[i]>pivot) i++;
            while(a[j]<pivot) j--;
            if(i<=j){
                h=a[i]; a[i]=a[j]; a[j]=h;//tukar
                i++; j--;
            }
        }while(i<=j);
        //pengurutan
        if(lo<j) quickSort(a, lo, j);
        if(i<hi) quickSort(a, i, hi);
    }
    public static void main(String[] args) {
       System.out.println(" DAMAR RAIHAN CHOIRUL FIRDAUS / XRPL6 / 15 ");
       System.out.println("\n ---QUICK SORT---");
       System.out.print("\n");
        
        int bil[]={8,2,9,1,5,3,7,4,10};
        int i,n=bil.length;
            System.out.println("Sebelum di sorting : ");
            for(i=0;i<n;i++){
                System.out.println(bil[i]+ " ");
            }
             System.out.print("\n");
             System.out.println("Setelah di sorting : ");
             quickSort(bil,0,n-1);
             for(i=0;i<n;i++){
                 System.out.println(bil[i]+" ");
             }
    }
    
}

